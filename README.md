datalife_timesheet_work_notebook
================================

The timesheet_work_notebook module of the Tryton application platform.

[![Build Status](https://drone.io/gitlab.com/datalifeit/trytond-timesheet_work_notebook/status.png)](https://drone.io/gitlab.com/datalifeit/trytond-timesheet_work_notebook/latest)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
